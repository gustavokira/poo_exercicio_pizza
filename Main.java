// 1) Comente o que faz cada linha do programa.
// 2) Faça com que um combo tenha um nome.
// 3) Crie mais 4 sabores de pizza. Dois devem ser doces.
// 4) Crie mais 2 tipos de bebida.
// 5) Crie mais 2 combos novos.
// 6) Mostre as informacoes de todos os combos criados.
// 7) Faça com que seja possível identificar se a bebida é alcólica.

class Main {
  public static void main(String[] args) {
    Sabor s1 = new Sabor();
    s1.nome = "4 queijos";
    s1.doce = false;

    Sabor s2 = new Sabor();
    s2.nome = "Marguerita";
    s2.doce = false;

    Pizza pA = new Pizza();
    pA.sabor1 = s1;
    pA.sabor2 = s2;
    pA.preco = 20.20;

    Bebida b = new Bebida();
    b.nome = "Coca cola";
    b.volume = 120;
    b.preco = 5.0;

    Combo combo1 = new Combo();
    combo1.pizza = pA;
    combo1.bebida = b;

    double precoDoCombo = combo1.pizza.preco + combo1.bebida.preco;

    System.out.println("combo:");
    System.out.println("sabores");
    System.out.println(combo1.pizza.sabor1.nome);
    System.out.println(combo1.pizza.sabor2.nome);
    System.out.println(combo1.bebida.nome);
    System.out.println(combo1.bebida.volume+" ml");
    System.out.println("preco: "+precoDoCombo+" reais");

  }
}

class Pizza{
  Sabor sabor1;
  Sabor sabor2;
  double preco;
}

class Sabor{
  boolean doce;
  String nome;
}

class Bebida{
  String nome;
  int volume;
  double preco;
}

class Combo{
  Pizza pizza;
  Bebida bebida;
}
